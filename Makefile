CFLAGS=--std=c17 -Isrc/ -ggdb
BUILDDIR=build
OBJ=obj
SRCDIR=src
CC=gcc
ASM=nasm
ASMFLAGS=-felf64 -g

all: $(OBJ)/main.o $(OBJ)/image.o $(OBJ)/file.o $(OBJ)/asm_sepia.o $(OBJ)/sepia.o
	$(CC) -o $(BUILDDIR)/sepia $^

build:
	mkdir -p $(BUILDDIR)
	
obj:
	mkdir -p $(OBJ)


$(OBJ)/asm_sepia.o: $(SRCDIR)/asm_sepia.asm obj
	$(ASM) $(ASMFLAGS) $< -o $@

$(OBJ)/main.o: $(SRCDIR)/main.c obj
	$(CC) -c $(CFLAGS) $< -o $@
  
$(OBJ)/image.o: $(SRCDIR)/image.c obj
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJ)/file.o: $(SRCDIR)/file.c obj
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJ)/sepia.o: $(SRCDIR)/sepia.c obj $(OBJ)/asm_sepia.o
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
