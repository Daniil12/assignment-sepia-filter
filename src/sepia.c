#include <include/sepia.h>

static float rgb[256];

static const float sepia_color[3][3] = {
        {.393f, .769f, .189f},
        {.349f, .686f, .168f},
        {.272f, .543f, .131f}
};

static unsigned char check(uint64_t x) {
    if (x > 255) {
        return 255;
    } else {
        return x;
    }
}

static void set_rgb() {
    for (size_t i = 0; i <= 256; i++) {
        rgb[i] = (float) i;
    }
}

// C

static void sepia_set_c(struct pixel *const pixel) {
    struct pixel const old = *pixel;
    pixel->r = check(rgb[old.r] * sepia_color[0][0] + rgb[old.g] * sepia_color[0][1] + rgb[old.b] * sepia_color[0][2]);

    pixel->g = check(rgb[old.r] * sepia_color[1][0] + rgb[old.g] * sepia_color[1][1] + rgb[old.b] * sepia_color[1][2]);

    pixel->b = check(rgb[old.r] * sepia_color[2][0] + rgb[old.g] * sepia_color[2][1] + rgb[old.b] * sepia_color[2][2]);
}

struct image sepia_c(struct image img) {
    set_rgb();
    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            sepia_set_c(&img.data[i * img.width + j]);
        }
    }
    return img;
}

// ASM

extern void asm_sepia(float buff[12]);

static void sepia_set_asm(struct pixel *const p) {
    float buff[12];
    for (size_t i = 0; i < 4; i++) {
        buff[i] = rgb[p->r];
        buff[i + 4] = rgb[p->g];
        buff[i + 8] = rgb[p->b];
    }
    asm_sepia(buff);
    p->r = check(buff[0]);
    p->g = check(buff[1]);
    p->b = check(buff[2]);
}

struct image sepia_asm(struct image img) {
    set_rgb();
    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j += 4) {
            sepia_set_asm(&img.data[i * img.width + j + 0]);
            sepia_set_asm(&img.data[i * img.width + j + 1]);
            sepia_set_asm(&img.data[i * img.width + j + 2]);
            sepia_set_asm(&img.data[i * img.width + j + 3]);
        }
    }
    return img;
}

