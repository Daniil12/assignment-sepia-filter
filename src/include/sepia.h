#include "image.h"

#include <stdlib.h>

#pragma once
struct image sepia_c(struct image img);
struct image sepia_asm(struct image img);
