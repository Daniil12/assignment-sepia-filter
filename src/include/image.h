#include <stdint.h>
#include <stdio.h>

#pragma once
struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y);

struct image *create_img(struct image *img, size_t width, size_t height);

void destroy(struct image* img);
