#include <include/image.h>

#include <stdlib.h>

void destroy(struct image *img) {
    free(img->data);
}

struct image *create_img(struct image *img, uint64_t width, uint64_t height) {
    img->height = height;
    img->width = width;
    img->data = malloc(width * height * sizeof(struct pixel));
    return img;
}
