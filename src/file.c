#include <include/file.h>
#include <include/bmp.h>

#include <stdlib.h>

static const uint32_t BF_TYPE_ONE = 0x4d42;
static const uint32_t BF_TYPE_TWO = 0x4349;
static const uint32_t BF_TYPE_THREE = 0x5450;
static const size_t BIT_COUNT = 24;
static const uint32_t BI_SIZE = 40;

size_t padding(uint64_t width) {
    return (4 - width * 3) % 4;
}

uint32_t bmp_size(struct image *img) {
    return (sizeof(struct pixel) * img->width * img->height + padding(img->width)) * img->height;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL || img == NULL) {
        return READ_ERROR;
    }

    struct bmp_header header;
    size_t res;
    res = fread(&header, 1, sizeof(struct bmp_header), in);

    if (res != sizeof(struct bmp_header)) {
        return INVALID_SIZE;
    } else if (header.bfType != BF_TYPE_ONE && header.bfType != BF_TYPE_TWO && header.bfType != BF_TYPE_THREE) {
        return READ_INVALID_SIGNATURE;
    } else if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    img = create_img(img, header.biWidth, header.biHeight);

    for (int i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, in) != 1) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding(img->width), 1) != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    if (out == NULL || img == NULL) {
        return WRITE_ERROR;
    }

    struct bmp_header header = (struct bmp_header) {
            .bfType = BF_TYPE_ONE,
            .bfileSize = sizeof(struct bmp_header) + bmp_size(img),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biHeight = img->height,
            .biWidth = img->width,
            .biPlanes = 1,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = bmp_size(img),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR_HEADERS;
    }

    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, out);
            fseek(out, padding(img->width), 1);
        }
    }

    return WRITE_OK;
}


