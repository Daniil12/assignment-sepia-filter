global asm_sepia

section .data
    r1: dd 0.393, 0.349, 0.272, 0.393
    r2: dd 0.769, 0.686, 0.543, 0.769
    r3: dd 0.189, 0.168, 0.131, 0.189

section .text

asm_sepia:
    mov rsi, 16
    add rsi, rdi
    mov rdx, 32
    add rdx, rdi
    push rdi
    push rsi
    push rdx
    movdqa xmm1, [rel r1]
    movdqa xmm2, [rel r2]
    movdqa xmm3, [rel r3]
    movdqu xmm4, [rdi]
    movdqu xmm5, [rsi]
    movdqu xmm6, [rdx]
    mulps  xmm4, xmm1
    mulps  xmm5, xmm2
    mulps  xmm2, xmm3
    addps  xmm4, xmm5
    addps  xmm4, xmm2
    movdqu [rdi], xmm4
    pop rdx
    pop rsi
    pop rdi
    ret
