#include "include/file.h"
#include "include/sepia.h"
#include "include/bmp.h"

#include <stdio.h>
#include <stdlib.h>

int error(const char *message) {
    fputs(message, stderr);
    return 0;
}

void info(const char *message) {
    fputs(message, stdout);
}

int main(int argc, char **argv) {
    if (argc != 4) {
        return error("Необходимо 3 параметра\n");
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        fclose(in);
        return error("Не удалось окрыть входной файл\n");
    }

    struct image img = {0};
    enum read_status rs = from_bmp(in, &img);
    if (rs != READ_OK) {
        destroy(&img);
        fclose(in);
        return error("Неверного формат bmp\n");
    }
    fclose(in);

    info("Входной файл успешно прочитан\n");

    struct image img_sepia = {0};
    if (*argv[3] == 'c') {
        img_sepia = sepia_c(img);
        info("Сепия была реализована на  C\n");
    } else if (*argv[3] == 'a') {
        img_sepia = sepia_asm(img);
        info("Сепия была реализована  asm\n");
    } else {
        return error("Последний аргумент введен неверно\n");
    }


    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        fclose(out);
        return error("Не удалось окрыть выходной файл\n");
    }

    enum write_status ws = to_bmp(out, &img_sepia);
    if (ws != WRITE_OK) {
        destroy(&img_sepia);
        fclose(out);
        return error("Ошибка конвертации в .bmp\n");
    }
    info("Заголовки успешно записаны\n");

    destroy(&img_sepia);
    fclose(out);
    return 0;
}
